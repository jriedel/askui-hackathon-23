# Askui Hackthon 2023

https://hackathon.askui.com/

## Installation of Jest and Typescript

Install the dependencies
```shell
npm install
```

Add your askui configuration to `test/helper/jest.setup.ts`.

Run the use case
```shell
npm run test
```
