import { aui } from './helper/jest.setup';

const registrationFormData = {
  email: 'mega@tester.de',
  firstName: 'Mega',
  lastName: 'Tester',
  companyName: 'Test Inc.',
  jobTitle: 'Automation Engineer',
  country: 'Germany',
  participationType: 'Individual',
  howIFoundOutAboutEventOptionsToSelect: ['LinkedIn', 'Twitter'],
}

const submissionFormData = {
  email: 'mega@tester.de',
  firstName: 'Mega',
  lastName: 'Tester',
  ideationAndApproach: 'Let\'s be honest, nobody likes to fill out forms but sometimes we need to, e.g. when wanting to take part in a askui hackathon. \
So I built an automation script with askui that allows me to register for the hackathon and submit a project automatically so that I can save some time and are more likely to submit something when I\'m not held back by the registration process. \
Also this can be used by the askui team in the future for the next hackathon as an example and additions help for the participants for registering and submitting.',
  goal: 'Automate annoying things to free up time to do the great stuff',
  repoUrl: '',
  driveUrl: 'https://drive.google.com/drive/folders/10_l_kMqyK7MwjYCYfnTbLxZPhQ2M9zZM'
}

describe('jest with askui', () => {
  it('auto-registers and submit', async () => {
    await aui.typeIn('askui hackathon how the hack').url().contains().icon().withText('search').exec()
    await aui.pressKey('enter').exec();
    await aui.click().text().withText('askui.com').exec();
    await aui.click().text().withText('Register now').exec();
    await aui.typeIn(registrationFormData.email).textfield().below().text().withText('Email').exec();
    await aui.pressKey('tab').exec();
    await aui.type(registrationFormData.firstName).exec();
    await aui.pressKey('tab').exec();
    await aui.type(registrationFormData.lastName).exec();
    await aui.pressKey('tab').exec();
    await aui.type(registrationFormData.companyName).exec();
    await aui.pressKey('tab').exec();
    await aui.type(registrationFormData.jobTitle).exec();
    await aui.pressKey('tab').exec();
    await aui.type(registrationFormData.country).exec();
    await aui.scroll(10, 0).exec();
    await aui.click().text().withText(registrationFormData.participationType).below().text().withText('How will you be participating?').exec();
    for (const option of registrationFormData.howIFoundOutAboutEventOptionsToSelect) {
      await aui.click().text().withText(option).below().text().withText('How did you find out about this event?').exec();
    }
    await aui.click().text().withText('I consent to receiving information about the current hackathon.').exec();
    await aui.moveMouseTo().button().withText('Submit').exec();
    await aui.click().button().withText('Submit').exec();

    await aui.typeIn(submissionFormData.repoUrl).url().exec()
    await aui.pressKey('enter').exec();
    await aui.click().icon().withText('info').exec();
    await aui.click().text().withText('Zugriff verwalten').exec();
    await aui.click().text().withText('Eingeschränkt').exec();
    await aui.click().text().withText('Jeder, der über den Link verfügt').exec();
    await aui.click().text().withText('Fertig').exec();

    await aui.typeIn('askui hackathon how the hack').url().exec()
    await aui.pressKey('enter').exec();
    await aui.click().text().withText('askui.com').exec();
    await aui.click().text().withText('FAQ').exec();
    await aui.scroll(100, 0).exec();
    await aui.moveMouseRelativelyTo(-300, 0).text().withText("submission form before the challenge However; you can get brownie points").exec();
    await aui.mouseLeftClick().exec();
    await aui.typeIn(submissionFormData.email).textfield().below().text().withText('Email').exec();
    await aui.pressKey('tab').exec();
    await aui.type(submissionFormData.firstName).exec();
    await aui.pressKey('tab').exec();
    await aui.type(submissionFormData.lastName).exec();
    await aui.pressKey('tab').exec();
    await aui.type(submissionFormData.ideationAndApproach).exec();
    await aui.pressKey('tab').exec();
    await aui.type(submissionFormData.goal).exec();
    await aui.pressKey('tab').exec();
    await aui.type(submissionFormData.repoUrl).exec();
    await aui.click().text().withText("ogree for my personal data to be shared with askui in order to receive information regording askui products,").exec();
    await aui.moveMouseTo().button().withText('Submit').exec();
    await aui.click().button().withText('Submit').exec();
  });
});
